import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarchantloginsuccessComponent } from './marchantloginsuccess.component';

describe('MarchantloginsuccessComponent', () => {
  let component: MarchantloginsuccessComponent;
  let fixture: ComponentFixture<MarchantloginsuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarchantloginsuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarchantloginsuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
