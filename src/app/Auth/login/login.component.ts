import { Component, OnInit } from '@angular/core';

import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UserService } from '../../Services/User.Services';
import { CommonService } from '../../../Shared/SharedServices/CommonService';
import * as $ from 'jquery';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [CommonService, UserService]
})
export class LoginComponent implements OnInit {
  isLoading: boolean = false;
  loginModel: any = {
    Email: '',
    Password: ''
  }
  constructor(private route: Router, private commonService: CommonService,
     private ToastrService: ToastrService, private userService: UserService) { }

  ngOnInit() {
   
    this.userService.logout();
    

  }

  loginSubmit(form: NgForm) {
    if (form.valid) {
      let UserExist: any = this.commonService.GetUserList().filter(i => i.UserName.toLowerCase() == this.loginModel.Email.toLowerCase() && i.Password == this.loginModel.Password);
      if (UserExist.length > 0) {
        this.isLoading = true;
        setTimeout((router: Router) => {
          this.isLoading = false;
          this.ToastrService.success("Login Successfully", "Login")
          this.userService.SaveUser(UserExist);
          this.route.navigate(['/dashboard'])
        }, 3000);
      }
      else {
        this.ToastrService.error("UserName or password is incorrect", "Login Error")

      }
    }
  }

}
