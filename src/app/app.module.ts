import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {ModalModule} from "ngx-modal";
import { AppHeaderComponent } from './Layout/app-header/app-header.component';
import { AppLayoutComponent } from './Layout/app-layout/app-layout.component';
import { SiteFooterComponent } from './Layout/site-footer/site-footer.component';
import { SiteLayoutComponent } from './Layout/site-layout/site-layout.component';
import { SiteHeaderComponent } from './Layout/site-header/site-header.component';
import { routing } from './app.routing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { DashboardComponent } from './dashboard/dashboard.component';

import { MerchantloginComponent } from './Auth/merchantlogin/merchantlogin.component';
import { LoginComponent } from './Auth/login/login.component';

import { MyWalletComponent } from './my-wallet/my-wallet.component';
import { LogoutComponent } from './logout/logout.component';
import { EligibilityComponent } from './Lead/eligibility/eligibility.component';
import { ProductDetailComponent } from './Lead/product-detail/product-detail.component';
import { CartDetailComponent } from './Lead/cart-detail/cart-detail.component';
import { PaymentmethodComponent } from './Lead/paymentmethod/paymentmethod.component';
import { BonuspageComponent } from './Lead/bonuspage/bonuspage.component';
import { AppFooterComponent } from './Layout/app-footer/app-footer.component';
import { IonRangeSliderModule } from "ng2-ion-range-slider";
import { ChartsModule } from 'ng2-charts';
import { RouterModule } from '@angular/router';
import { MarchantloginsuccessComponent } from './Auth/marchantloginsuccess/marchantloginsuccess.component';
import { MyDatePickerModule } from 'mydatepicker';
import {  ReactiveFormsModule } from '@angular/forms';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([
  
], { useHash: true });

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    AppLayoutComponent,
    SiteFooterComponent,
    SiteLayoutComponent,
    SiteHeaderComponent,
    LoginComponent,
    DashboardComponent,
    MyWalletComponent,
    MerchantloginComponent,
    MyWalletComponent,
    LogoutComponent,
    EligibilityComponent,
    ProductDetailComponent,
    CartDetailComponent,
    PaymentmethodComponent,
    BonuspageComponent,
    AppFooterComponent,
    MarchantloginsuccessComponent
    
    
    
    
  ],
  imports: [
    BrowserModule,routing,FormsModule,
    ModalModule,
    ReactiveFormsModule,
    IonRangeSliderModule,
    BrowserAnimationsModule,
    ChartsModule,
    rootRouting,
    MyDatePickerModule,
     // required animations module
    ToastrModule.forRoot() // ToastrModule added
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
