import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BonuspageComponent } from './bonuspage.component';

describe('BonuspageComponent', () => {
  let component: BonuspageComponent;
  let fixture: ComponentFixture<BonuspageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BonuspageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonuspageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
